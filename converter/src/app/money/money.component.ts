import { Component, OnInit } from '@angular/core';
import {Money} from "../model/money";
import {MoneiesServiceService} from "../service/moneies-service.service";
import {FormGroup,FormBuilder}  from "@angular/forms";
import {isNumeric} from "rxjs/internal-compatibility";
import {Resp} from "../model/resp";

@Component({
  selector: 'app-money',
  templateUrl: './money.component.html',
  styleUrls: ['./money.component.css']
})
export class MoneyComponent implements OnInit {
  moneyForms!: FormGroup;

  constructor(
    private moneyService: MoneiesServiceService,
    private fb: FormBuilder) {
  }

  moneies!: Money[]
  youMoney!: Money
  convertMoney!: Money
  rightConvert!: number
  leftConvert!: number
  respon: Resp ={
    rightConv:' ',
    leftConv:' '

  }


  ngOnInit(): void {
    this.moneyForms = this.fb.group({
        coust: 1,
        youMoney: 'USD',
        convertedMoney: 'USD'
      }

    )

    this.moneyService.getMoneies().subscribe(data => {
      console.log(data)
      this.moneies = data
    })
  }


  converte(): void {
console.log(this.moneies)
    for (let j in this.moneies) {

      if (this.moneies[j].code == this.moneyForms.value.youMoney) {

        this.youMoney = this.moneies[j]
      }
      if (this.moneies[j].code == this.moneyForms.value.convertedMoney) {

        this.convertMoney = this.moneies[j]
      }
    }
console.log(this.youMoney.value)
    console.log(this.convertMoney.value)
    this.rightConvert = this.youMoney.value / this.convertMoney.value * this.moneyForms.value.coust
    this.leftConvert = this.convertMoney.value / this.youMoney.value

    this.respon.rightConv = this.moneyForms.value.coust  +  " "  + this.youMoney.name + '= ' + this.rightConvert + '' + this.convertMoney.name;
    this.respon.leftConv = '1' + this.convertMoney.name + '=' + this.leftConvert + '' + this.youMoney.name;

  }
}
