import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpClientModule} from "@angular/common/http";
import {Observable} from "rxjs";
import {Money} from "../model/money";
import {Parser} from 'xml2js';
import * as path from "path";
// import {xml2js} from 'xml2js';




@Injectable({
  providedIn: 'root'

})

export class MoneiesServiceService {


  constructor(
    @Inject('valutaApiUrl') private apiUrl:string,
    private  httpClient:HttpClient
  ) {}
  moneies!:Money[]



  getMoneies():Observable<Money[]>{

    console.log("salam");
    console.log(this.httpClient.get<any>(this.apiUrl));


    return this.httpClient.get<Money[]>(this.apiUrl);
    }


}
