import { TestBed } from '@angular/core/testing';

import { MoneiesServiceService } from './moneies-service.service';

describe('MoneiesServiceService', () => {
  let service: MoneiesServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoneiesServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
